package com.transformhub.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.transformhub.dto.PostDto;
import com.transformhub.model.Post;
import com.transformhub.service.PostService;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/v1")
public class PostController {

	@Autowired
	private PostService postService;

	@GetMapping("/posts")
	public ResponseEntity<List<Post>> getAllposts(@RequestParam(required = false) String title) {
		List<Post> posts = postService.getAllposts(title);
		if (posts.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(posts, HttpStatus.OK);
	}

	@GetMapping("/posts/{id}")
	public ResponseEntity<Post> getpostById(@PathVariable("id") Long postId) {
		Post post = postService.findPostById(postId);
		return new ResponseEntity<>(post, HttpStatus.OK);
	}

	@PostMapping("/posts")
	public ResponseEntity<Post> createpost(@RequestBody PostDto post) {
		Post entity = postService.savePost(post);
		return new ResponseEntity<>(entity, HttpStatus.CREATED);
	}

	@PutMapping("/posts/{postId}")
	public ResponseEntity<Post> updatepost(@PathVariable("postId") Long postId, @RequestBody PostDto postDto) {
		return new ResponseEntity<>(postService.updatePost(postId, postDto), HttpStatus.OK);
	}

	@DeleteMapping("/posts/{postId}")
	public ResponseEntity<String> deletepost(@PathVariable("postId") Long postId) {
		postService.deletePost(postId);
		return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
	}
}
