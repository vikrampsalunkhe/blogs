package com.transformhub.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.transformhub.exception.ResourceNotFoundException;
import com.transformhub.model.Post;
import com.transformhub.model.Tag;
import com.transformhub.service.PostService;
import com.transformhub.service.TagService;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/v1")
public class TagController {

	@Autowired
	private PostService postService;

	@Autowired
	private TagService tagService;

	@GetMapping("/tags")
	public ResponseEntity<List<Tag>> getAllTags() {
		List<Tag> tags = tagService.getAllTags();
		if (tags.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(tags, HttpStatus.OK);
	}

	@GetMapping("/post/{postId}/tags")
	public ResponseEntity<List<Tag>> getAllTagsByPostId(@PathVariable(value = "postId") Long postId) {
		Post post = postService.findPostById(postId);
		if (post == null) {
			throw new ResourceNotFoundException("Not found Post with id = " + postId);
		}
		return new ResponseEntity<>(tagService.findByPostId(post), HttpStatus.OK);
	}

	@GetMapping("/tags/{tagId}")
	public ResponseEntity<Tag> getTagsById(@PathVariable(value = "tagId") Long tagId) {
		Tag tag = tagService.findTagById(tagId);
		return new ResponseEntity<>(tag, HttpStatus.OK);
	}

	@GetMapping("/tags/{tagId}/posts")
	public ResponseEntity<List<Post>> getAllTutorialsByTagId(@PathVariable(value = "tagId") Long tagId) {
		Tag tag = tagService.findTagById(tagId);
		if ( tag == null) {
			throw new ResourceNotFoundException("Not found Tag  with id = " + tagId);
		}
		List<Post> posts = postService.findByTagsId(tag);
		return new ResponseEntity<>(posts, HttpStatus.OK);
	}

	@PostMapping("/post/{postId}/tags")
	public ResponseEntity<String> addTag(@PathVariable(value = "postId") Long postId, @RequestBody Tag tagRequest) {
		tagService.addTagToPost(postId, postId);
		return new ResponseEntity<>("Tag added to post", HttpStatus.CREATED);
	}

	@PutMapping("/tags/{tagId}")
	public ResponseEntity<Tag> updateTag(@PathVariable("tagId") Long tagId, @RequestBody Tag tagRequest) {
		Tag tag = tagService.findTagById(tagId);
		if(tag==null) {
			throw new ResourceNotFoundException("Not found Tag  with id = " + tagId);
		}
		tag.setLabel(tagRequest.getLabel());
		return new ResponseEntity<>(tagService.saveTag(tag), HttpStatus.OK);
	}

	@DeleteMapping("/tags/{tagId}")
	public ResponseEntity<String> deleteTag(@PathVariable("tagId") Long tagId) {
		tagService.deleteTag(tagId);
		return new ResponseEntity<>("Tag Removed successfully", HttpStatus.NO_CONTENT);
	}
}
