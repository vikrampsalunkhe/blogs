package com.transformhub.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.transformhub.exception.ResourceNotFoundException;
import com.transformhub.model.Post;
import com.transformhub.model.Tag;
import com.transformhub.repository.PostRepository;
import com.transformhub.repository.TagRepository;

@Service
public class TagService {

	@Autowired
	private TagRepository tagRepository;

	@Autowired
	private PostService postService;
	
	@Autowired
	private PostRepository postRepository;

	public List<Tag> getAllTags() {
		return tagRepository.findAll();
	}

	public List<Tag> findByPostId(Post postId) {
		return tagRepository.findByPosts(postId);
	}

	public Tag findTagById(Long tagId) {
		return tagRepository.findById(tagId)
				.orElseThrow(() -> new ResourceNotFoundException("Not found Tag with id = " + tagId));
	}
	
	public Tag findTagByLabel(String label) {
		return tagRepository.findByLabelContainingIgnoreCase(label);
	}

	public Tag saveTag(Tag tag) {
		return tagRepository.save(tag);
	}

	public void deleteTag(Long tagId) {
		Tag tag = findTagById(tagId);
		List<Post> posts = postService.findByTagsId(tag);
		posts.stream().forEach(post -> {
			post.removeTag(tagId);
			postRepository.save(post);
		});
		tagRepository.deleteById(tagId);
	}

	public void addTagToPost(Long postId, Long tagId) {
		Tag tag = findTagById(tagId);

		if (tag != null) {
			Post post = postService.findPostById(postId);
			if (post != null) {
				post.addTag(tag);
				postRepository.save(post);
			}
		}
	}

}
