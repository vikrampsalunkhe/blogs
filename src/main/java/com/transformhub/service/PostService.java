package com.transformhub.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.transformhub.dto.PostDto;
import com.transformhub.exception.ResourceNotFoundException;
import com.transformhub.model.Post;
import com.transformhub.model.Tag;
import com.transformhub.repository.PostRepository;

@Service
public class PostService {

	@Autowired
	private PostRepository postRepository;

	@Autowired
	@Lazy
	private TagService tagService;

	public List<Post> getAllposts(String title) {
		if (title == null)
			return postRepository.findAll();
		else
			return postRepository.findByTitleContaining(title);
	}

	public Post findPostById(Long id) {
		return postRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Not found post with id = " + id));
	}

	public Post savePost(PostDto postDto) {
		ModelMapper  modelMapper = new ModelMapper();
		Post post= modelMapper.map(postDto, Post.class);
		Set<Tag> tags = new HashSet<>();
		for (String tag : postDto.getTags()) {
			Tag tagExisted = tagService.findTagByLabel(tag);
			if (tagExisted == null) {
				tagExisted = new Tag();
				tagExisted.setLabel(tag);
			}  
			tags.add(tagExisted);
		}
		post.setTags(tags);
		return postRepository.save(post);
	}

	public Post updatePost(Long postId, PostDto postDto) {
		ModelMapper  modelMapper = new ModelMapper();
		Post entity = findPostById(postId);
		modelMapper.map(postDto,entity);
		Set<Tag> tags = new HashSet<>();
		for (String tag : postDto.getTags()) {
			Tag tagExisted = tagService.findTagByLabel(tag);
			if (tagExisted == null) {
				tagExisted = new Tag();
				tagExisted.setLabel(tag);
			}  
			tags.add(tagExisted);
		}
		entity.setTags(tags);
		return postRepository.save(entity);

	}

	public void deletePost(Long postId) {
		Post post = findPostById(postId);
		postRepository.delete(post);
	}

	public List<Post> findByTagsId(Tag tag) {
		return postRepository.findByTags(tag);
	}

}
