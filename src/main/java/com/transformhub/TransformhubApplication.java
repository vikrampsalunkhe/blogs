package com.transformhub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransformhubApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransformhubApplication.class, args);
	}

}
