package com.transformhub.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.transformhub.model.Post;
import com.transformhub.model.Tag;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
	
  List<Post> findByTitleContaining(String title);
  
  List<Post> findByTags(Tag tagId);
}
