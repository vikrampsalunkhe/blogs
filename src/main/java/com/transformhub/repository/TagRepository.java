package com.transformhub.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.transformhub.model.Post;
import com.transformhub.model.Tag;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
  List<Tag> findByPosts(Post postId);
  
  Tag findByLabelContainingIgnoreCase(String label);
}
