package com.transformhub.dto;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostDto {

	@Column(name = "title", length = 1000)
	private String title;

	@Column(name = "content", length = 100000)
	private String content;

	private Set<String> tags = new HashSet<>();

}
